//
//  PostTableViewController.swift
//  homework3
//
//  Created by Sereyvong Thorn on 11/23/18.
//  Copyright © 2018 Sereyvong Thorn. All rights reserved.
//

import UIKit

class PostTableViewController: UITableViewController {

    var listPosts:[Post]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listPosts = Post.listPosts()
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (listPosts?.count)!
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostTableViewCell
        
        cell.configureCell(post: (listPosts?[indexPath.row])!)
        
        return cell
    }


}
